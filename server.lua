local raceSet = false

AddEventHandler('chatMessage', function(s, n, m)
    local message = string.lower(m)
    if message == "/setrace" then
        CancelEvent()
        --------------
        TriggerClientEvent('setrace', s)
        raceSet = true
    end
end)

AddEventHandler('chatMessage', function(s, n, m)
    local message = string.lower(m)
    if message == "/startrace" then
        if raceSet then
            CancelEvent()
            --------------
            TriggerClientEvent('startrace', s)
        else
        end
    end
end)