--[[
    
    Tim Wick's Racing script for dank racing

]]--
local raceSet = false
local playerPed = PlayerPedId()
local coords = GetEntityCoords(playerPed)
local isRacing = false
local finishedRace = true
local startTime

function LocalPed()
    return GetPlayerPed(-1)
end

RegisterNetEvent('setrace')
AddEventHandler('setrace',function() 
    local player = GetPlayerPed(-1)
    
    if (IsPedSittingInAnyVehicle(player)) then 
       
        local blip = GetFirstBlipInfoId(8)
          
        if blip ~= nil then
            SetBlipSprite(blip, 315)
             
            raceSet = true
            ShowNotification("~g~Race is set!")
        end
    else
        ShowNotification("~r~You must be in a vehicle to do that")
    end
end)

RegisterNetEvent('startrace')
AddEventHandler('startrace',function() 
    local player = GetPlayerPed(-1)
    if isRacing then
        ShowNotification("~r~You must finish your race first!")
    end
    if raceSet == false then
        ShowNotification("~r~You must set your race!")
    end
    if (IsPedSittingInAnyVehicle(player)) then 

        if raceSet then
            PlaySoundFrontend(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET" )
            Citizen.CreateThread(function()
                local time = 0
                function setcountdown(x)
                  time = GetGameTimer() + x*1000
                end
                function getcountdown()
                  return math.floor((time-GetGameTimer())/1000)
                end
                setcountdown(5)  -- The Seconds that count down
                while getcountdown() > 0 do
                    Citizen.Wait(1)
                    FreezeEntityPosition(GetVehiclePedIsUsing(player), true) 
                    DrawHudText(getcountdown(), {255,191,0,255},0.48,0.4,4.0,4.0)
                    FreezeEntityPosition(GetVehiclePedIsUsing(player), false) -- move this one down 1 line to not have "burnout", but 100% freeze
                end
                isRacing = true
                TriggerRaceBegin ()
            end)
        end
    end
end)

function TriggerRaceBegin ()
    startTime = GetGameTimer()
    Citizen.CreateThread(function() 
        ShowNotification("~g~Race is started!")
       local blipid = 8
       local blip = GetFirstBlipInfoId(blipid)
        while isRacing do  
            Citizen.Wait(5)
            
            --Cancelling the race when F1, F2 or F3 is pressed:
            if (IsControlJustReleased(1, 288) or IsDisabledControlJustReleased( 0, 288 )) then
                finishedRace = false
                ShowNotification("~r~You cancelled the race")
                TriggerRaceEnd ()
            end 
            if (IsControlJustReleased(1, 289) or IsDisabledControlJustReleased( 0, 289 )) then
                finishedRace = false
                ShowNotification("~r~You cancelled the race")
                TriggerRaceEnd ()
            end 
            if (IsControlJustReleased(1, 170) or IsDisabledControlJustReleased( 0, 170 )) then
                finishedRace = false
                ShowNotification("~r~You cancelled the race")
                TriggerRaceEnd ()
            end
        
            blip = GetFirstBlipInfoId(315)
            
            if not DoesBlipExist(blip) then
                finishedRace = false
                ShowNotification("~r~You finished the race! Good Job!")
                TriggerRaceEnd ()
            end
        end
    end)
end

function TriggerRaceEnd ()
    if finishedRace == false then
        Citizen.Wait(0) 
        PlaySoundFrontend(-1, "ScreenFlash", "WastedSounds")
        --PlaySoundFrontend(-1, "CHECKPOINT_PERFECT", "HUD_MINI_GAME_SOUNDSET")
        isRacing = false
        raceSet = false
        finishedRace = true
        removeBlip(blip)
    end
end
--utility funcs

function ShowNotification( text )
    SetNotificationTextEntry( "STRING" )
    AddTextComponentString( text )
    DrawNotification( false, false )
end


function Draw3DText(x,y,z,textInput,fontId,scaleX,scaleY)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)
    
    local scale = (1/dist)*20
    local fov = (1/GetGameplayCamFov())*100
    local scale = scale*fov
    
    SetTextScale(scaleX*scale, scaleY*scale)
    SetTextFont(fontId)
    SetTextProportional(1)
    SetTextColour(255, 255, 255, 250)
    SetTextDropshadow(1, 1, 1, 1, 255)
    SetTextEdge(2, 0, 0, 0, 150)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    SetTextCentre(1)
    AddTextComponentString(textInput)
    SetDrawOrigin(x,y,z+2, 0)
    DrawText(0.0, 0.0)
    ClearDrawOrigin()
end

function DrawHudText(text,colour,coordsx,coordsy,scalex,scaley) --courtesy of driftcounter
    SetTextFont(4) -- 7 is the heavy GTA font
    SetTextProportional(7)
    SetTextScale(scalex, scaley)
    local colourr,colourg,colourb,coloura = table.unpack(colour)
    SetTextColour(colourr,colourg,colourb, coloura)
    SetTextDropshadow(0, 0, 0, 0, coloura)
    SetTextEdge(1, 0, 0, 0, coloura)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(coordsx,coordsy)
end


function drawTxt(x,y ,width,height,scale, text, r,g,b,a)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end