local DestinationBlip           = nil
local TargetCoords              = nil

Config.JobLocations = {
    {x = 293.476,  y = -590.163, z = 42.7371}
}

TargetCoords = Config.JobLocations[GetRandomIntInRange(1, #Config.JobLocations)]
distance = GetDistanceBetweenCoords(playerCoords, TargetCoords.x, TargetCoords.y, TargetCoords.z, true)

local street = table.pack(GetStreetNameAtCoord(TargetCoords.x, TargetCoords.y, TargetCoords.z))
local msg    = nil

if street[2] ~= 0 and street[2] ~= nil then
    msg = string.format(_U('take_me_to_near', GetStreetNameFromHashKey(street[1]), GetStreetNameFromHashKey(street[2])))
else
    msg = string.format(_U('take_me_to', GetStreetNameFromHashKey(street[1])))
end

local blip = GetFirstBlipInfoId(8)
local blipX = 0.0
local blipY = 0.0

if (blip ~= 0) then
    local coord = GetBlipCoords(blip)
    blipX = coord.x
    blipY = coord.y
end

ESX.ShowNotification(msg)
DestinationBlip = AddBlipForCoord(TargetCoords.x, TargetCoords.y, TargetCoords.z)

BeginTextCommandSetBlipName("STRING")
AddTextComponentSubstringPlayerName("Destination")
EndTextCommandSetBlipName(blip)
SetBlipRoute(DestinationBlip, true)